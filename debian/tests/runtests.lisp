(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "global-vars")
  (asdf:load-system "global-vars-test"))

; The following raises an error if a test fails
(asdf:test-system "global-vars-test")
